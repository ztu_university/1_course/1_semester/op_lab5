#include <stdio.h>
#include <windows.h>

int main(void)
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	printf("Goodbye, World!\n");

	int y = -10;
	int x = 5;

	printf("x*y = %d\n", x*y);
	return 0;
}
